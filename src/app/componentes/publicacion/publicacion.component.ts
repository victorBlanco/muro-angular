import { Component, OnInit } from '@angular/core';
import { Publicacion } from '../../interfaces/publicaciones';
import { PublicacionesService } from '../../services/publicaciones.service';


@Component({
  selector: 'app-publicacion',
  templateUrl: './publicacion.component.html',
  styleUrls: ['./publicacion.component.css']
})
export class PublicacionComponent implements OnInit {
  publicacion:Publicacion;

  //nuevo
  public listPublicaciones:any=[];
  constructor(private publicacionService: PublicacionesService) {
    //nuevvo
    //this.publicacionService.getPublicaciones().subscribe(resp=>{
    //  console.log(resp);
    //  });
   }

   /**
    *    getAllPublicaciones (){
    this.publicacionService.getPublicaciones()
    .subscribe(todos => {
      console.log(todos);
    });
  }
    */


  ngOnInit(): void {
    //nuevo 
    this.cargarDatos();
  }
  public cargarDatos(){
    this.publicacionService.get('http://127.0.0.1:8000/api/publicaciones')
    .subscribe(respuesta=>{
      this.listPublicaciones=respuesta;
  })
  }
  

}
