import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PublicacionesService } from '../../services/publicaciones.service';

@Component({
  selector: 'app-comentario',
  templateUrl: './comentario.component.html',
  styleUrls: ['./comentario.component.css']
})
export class ComentarioComponent implements OnInit {
  public form:FormGroup;

  constructor(private publicacionService: PublicacionesService, private formBuilder:FormBuilder) { }

  ngOnInit(): void {

   


    this.form=this.formBuilder.group({
    // titulo:['',Validators.required],
    //  contenido:['']
    textAreaComentario:[''],
    titulo:['']
    })
  }

  


  public enviarData(){
    this.publicacionService.post('http://127.0.0.1:8000/api/publicaciones',
    {
      text:this.form.value.textAreaComentario
     // text:this.form.value.titulo

    }).subscribe(respuesta=>{
      console.log('comentario Enviado');
    })
  }

}
