import { Component, OnInit } from '@angular/core';
import { PublicacionesService } from '../../services/publicaciones.service';
import { HttpClient } from '@angular/common/http';
import { Publicacion } from '../../interfaces/publicaciones';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public listPublicaciones:any=[];
  public comentarios:any = [];
  comentarioText:string;
  public form: FormGroup;


  API_ENDPOINT='http://127.0.0.1:8000/api';
  //publicaciones:Publicacion;

//constructor(private publicacionService: PublicacionesService,private httpClient:HttpClient) { 

  constructor(private publicacionService: PublicacionesService,private route:ActivatedRoute,  private formBuilder: FormBuilder) { 


   // this.httpClient.get(this.API_ENDPOINT+'/publicaciones').subscribe((data:Publicacion[])=>{
   //   this.publicaciones=data; 
   /**
    * this.publicacionService.getPublicaciones().subscribe(resp=>{
     console.log(resp);
     });
    */
  }


  /**
   * 
   * getAllPublicaciones (){
    this.publicacionService.getPublicaciones()
    .subscribe(todos => {
      console.log(todos);
    });
  }
   */
  

  

  ngOnInit(): void {
   // this.getAllPublicaciones();

   this.route.paraMap.subscibe((paraMap:any)=>
   const{params}=paraMap
   this.cargarDatos(params.variable);
   this.cargarComentarios();
   
   )
 
    
  }
  public cargarDatos(){
    this.publicacionService.get('http://127.0.0.1:8000/api/publicaciones')
    .subscribe(respuesta=>{
      this.listPublicaciones=respuesta;
  })
  }

  public cargarComentarios(){

  }
  public post(url:string,body:any){
    return this.post(url,body);
  }
  



}
