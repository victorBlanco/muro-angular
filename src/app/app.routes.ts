import { RouterModule ,Routes} from '@angular/router';
import { HomeComponent } from './componentes/home/home.component';


//agregar las otras rutas al final
const APP_Routes:Routes=[
    {path:'home',component:HomeComponent},
    {path:'**', pathMatch:'full',redirectTo:''}

];
export const APP_ROUTING=RouterModule.forRoot(APP_Routes);