import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from "@angular/common/http";

import { Publicacion } from '../interfaces/publicaciones';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class PublicacionesService {
 // _url='http://127.0.0.1:8000/api/publicaciones';

  constructor(private http: HttpClient ) {
   // console.log("servicio activado");
 
   }

   /**
    * 
    * @returns 
    * 
    * getPublicaciones(){
  let header=new HttpHeaders().set('Type-content','aplication/json')
  return this.http.get(this._url,{
    headers:header
  });

}
    */


/**
getPublicaciones() {
  const path = 'http://127.0.0.1:8000/api/publicaciones';
  return this.http.get<Publicacion[]>(path);
}
 */

public get(url:string){
  return this.http.get(url);
}

public post(url:string,body){
  return this.post(url,body);
}


 
}
