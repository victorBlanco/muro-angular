import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppComponent } from './app.component';
import { NavbarComponent } from './componentes/navbar/navbar.component';
import { PublicacionComponent } from './componentes/publicacion/publicacion.component';
import { ComentarioComponent } from './componentes/comentario/comentario.component';
import { DestacadoComponent } from './componentes/destacado/destacado.component';
import { PerfilComponent } from './componentes/perfil/perfil.component';


//Rutas
import {APP_ROUTING} from './app.routes';




/*Agregaron las librerias propias  para hacer la funcionalidad y recibir api de laravel*/
import {HttpClientModule} from '@angular/common/http';

import { HomeComponent } from './componentes/home/home.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PublicacionesService } from './services/publicaciones.service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    PublicacionComponent,
    ComentarioComponent,
    DestacadoComponent,
    PerfilComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule, //
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    PublicacionesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
