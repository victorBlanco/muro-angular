import { Component, OnInit } from '@angular/core';

//nuevo
import { PublicacionesService } from './services/publicaciones.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'prueba-muro';
  public listPublicaciones:any=[];

  constructor(
    private publicacionesService : PublicacionesService){

    }

  /**
   * getAllPublicaciones (){
    this.publicacionesService.getPublicaciones()
    .subscribe(todos => {
      console.log(todos);
    });
  }
   */

  ngOnInit():void{
    this.cargarDatos();

  }

  public cargarDatos(){
    this.publicacionesService.get('http://127.0.0.1:8000/api/publicaciones')
    .subscribe(respuesta=>{
      this.listPublicaciones=respuesta;
  })
  }






 
}
